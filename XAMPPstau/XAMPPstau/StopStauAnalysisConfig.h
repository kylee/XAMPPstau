#ifndef XAMPPStau_StopStauAnalysisConfig_h
#define XAMPPStau_StopStauAnalysisConfig_h
#include <XAMPPbase/AnalysisConfig.h>

namespace XAMPP {
    class StopStauAnalysisConfig : public AnalysisConfig {
    public:
        ASG_TOOL_CLASS(StopStauAnalysisConfig, XAMPP::IAnalysisConfig)
        StopStauAnalysisConfig(std::string Analysis = "Staus");
        virtual ~StopStauAnalysisConfig() = default;

    protected:
        virtual StatusCode initializeCustomCuts();
    };
}  // namespace XAMPP
#endif
