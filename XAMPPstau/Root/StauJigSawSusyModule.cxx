#include <XAMPPbase/AnalysisUtils.h>
#include <XAMPPbase/ToolHandleSystematics.h>

#include <XAMPPstau/AnalysisUtils.h>
#include <XAMPPstau/StauJigSawSusyModule.h>

namespace XAMPP {
    StauJigSawSusyModule::~StauJigSawSusyModule() {}
    StauJigSawSusyModule::StauJigSawSusyModule(const std::string& myname) :
        StauJigSawZModule(myname),
        m_vis_Tau1(nullptr),
        m_vis_Tau2(nullptr),
        m_nu1(nullptr),
        m_nu2(nullptr),
        m_Tau1(nullptr),
        m_Tau2(nullptr),
        m_Stau1(nullptr),
        m_Stau2(nullptr),
        m_Nino1(nullptr),
        m_Nino2(nullptr),
        m_Stau1Stau2(nullptr),
        m_comb_jigsaw(nullptr),
        m_min_dM_tau1(nullptr),
        m_min_dM_tau2(nullptr) {}
    unsigned int StauJigSawSusyModule::flavour() const { return JigSawFlavour::Susy; }

    StatusCode StauJigSawSusyModule::setupRJanalysis() {
        m_vis_Tau1 = std::make_shared<VisibleRecoFrame>("tau1_vis", "#tau_{a}^{vis}");
        m_vis_Tau2 = std::make_shared<VisibleRecoFrame>("tau2_vis", "#tau_{b}^{vis}");

        m_nu1 = std::make_shared<InvisibleRecoFrame>("nu1", "#nu_{a}");
        m_nu2 = std::make_shared<InvisibleRecoFrame>("nu2", "#nu_{b}");

        m_Tau1 = std::make_shared<DecayRecoFrame>("tau1", "#tau_{a} #to #tau_{a}^{vis}#nu_{a}");
        m_Tau2 = std::make_shared<DecayRecoFrame>("tau2", "#tau_{b} #to #tau_{b}^{vis}#nu_{b}");

        m_Tau1->AddChildFrame(*m_vis_Tau1);
        m_Tau1->AddChildFrame(*m_nu1);

        m_Tau2->AddChildFrame(*m_vis_Tau2);
        m_Tau2->AddChildFrame(*m_nu2);

        m_Nino1 = std::make_shared<InvisibleRecoFrame>("nino1", "#tilde{#chi}_{1a}^{0}");
        m_Nino2 = std::make_shared<InvisibleRecoFrame>("nino2", "#tilde{#chi}_{1b}^{0}");

        m_Stau1 = std::make_shared<DecayRecoFrame>("Stau1", "#tilde{#tau}_{a} #to #tau_{a}^{vis}#tilde{#chi}_{1,a}^{0}");
        m_Stau2 = std::make_shared<DecayRecoFrame>("Stau2", "#tilde{#tau}_{b} #to #tau_{b}^{vis}#tilde{#chi}_{1,b}^{0}");

        m_Stau1->AddChildFrame(*m_Tau1);
        m_Stau1->AddChildFrame(*m_Nino1);

        m_Stau2->AddChildFrame(*m_Tau2);
        m_Stau2->AddChildFrame(*m_Nino2);

        m_Stau1Stau2 = std::make_shared<DecayRecoFrame>("Stau1Stau2", "#tilde{#tau}_{a}#tilde{#tau}_{b}");
        m_Stau1Stau2->AddChildFrame(*m_Tau1);
        m_Stau1Stau2->AddChildFrame(*m_Tau2);

        m_lab_frame = std::make_shared<RestFrames::LabRecoFrame>("LabFrame", "LabFrame");
        m_lab_frame->SetChildFrame(*m_Stau1Stau2);
        if (!m_lab_frame->InitializeTree()) return StatusCode::FAILURE;

        ATH_MSG_INFO("Decay tree successfully built");
        m_InvisGroup = std::make_shared<InvisibleGroup>("Invisible", "#nu - jigsaws");
        m_InvisGroup->AddFrame(*m_nu1);
        m_InvisGroup->AddFrame(*m_nu2);
        m_InvisGroup->AddFrame(*m_Nino1);
        m_InvisGroup->AddFrame(*m_Nino2);
        ATH_MSG_INFO("Invisible group built");

        m_inivisblemass = std::make_shared<SetMassInvJigsaw>("NuNuM_R", "M_{\nino} = f(M_{\tau})");
        m_InvisGroup->AddJigsaw(*m_inivisblemass);

        m_rapidity = std::make_shared<SetRapidityInvJigsaw>("NuNuM_R", "M_{4 #nu} = f(4 #it{l})");
        m_InvisGroup->AddJigsaw(*m_inivisblemass);

        std::shared_ptr<ContraBoostInvJigsaw> stau_mass = std::make_shared<ContraBoostInvJigsaw>("Stau_masses", "M_{stau1} = M_{stau2}");
        stau_mass->AddInvisibleFrames(m_Stau1->GetListInvisibleFrames(), 0);
        stau_mass->AddInvisibleFrames(m_Stau2->GetListInvisibleFrames(), 1);
        stau_mass->AddVisibleFrames(m_Stau1->GetListVisibleFrames(), 0);
        stau_mass->AddVisibleFrames(m_Stau2->GetListVisibleFrames(), 1);
        m_contraboost = stau_mass;
        m_InvisGroup->AddJigsaw(*m_contraboost);

        std::shared_ptr<MinMassesSqInvJigsaw> min_tau_mass =
            std::make_shared<MinMassesSqInvJigsaw>("MinMWa_R", "min M_{#tau}, M_{#tau}^{1}= M_{#tau}^{b}", 2);
        min_tau_mass->AddInvisibleFrames(m_Tau1->GetListInvisibleFrames(), 0);
        min_tau_mass->AddInvisibleFrames(m_Tau2->GetListInvisibleFrames(), 1);
        min_tau_mass->AddVisibleFrames(m_Tau1->GetListVisibleFrames(), 0);
        min_tau_mass->AddVisibleFrames(m_Tau2->GetListVisibleFrames(), 1);
        min_tau_mass->AddMassFrame(*m_vis_Tau1, 0);
        min_tau_mass->AddMassFrame(*m_vis_Tau2, 1);
        m_min_deltaM = min_tau_mass;
        m_InvisGroup->AddJigsaw(*m_min_deltaM);

        //~ INV_R.AddJigsaw(MinMWa_R);
        //~ MinMWa_R.AddInvisibleFrames(Waa_R.GetListInvisibleFrames(), 0);
        //~ MinMWa_R.AddInvisibleFrames(Wab_R.GetListInvisibleFrames(), 1);
        //~ MinMWa_R.AddVisibleFrames(Waa_R.GetListVisibleFrames(), 0);
        //~ MinMWa_R.AddVisibleFrames(Wab_R.GetListVisibleFrames(), 1);
        //~ MinMWa_R.AddMassFrame(Lba_R, 0);
        //~ MinMWa_R.AddMassFrame(Lbb_R, 1);

        if (!m_lab_frame->InitializeAnalysis()) return StatusCode::FAILURE;

        return StatusCode::SUCCESS;
    }
    StatusCode StauJigSawSusyModule::fill() {
        xAOD::IParticleContainer* electrons = m_electrons->Container();
        xAOD::IParticleContainer* muons = m_muons->Container();
        xAOD::IParticleContainer* taus = m_taus->Container();

        if (!m_ParticleConstructor->HasSubContainer(full_name("JigSawParticles"))) {
            ATH_CHECK(m_ParticleConstructor->CreateSubContainer(full_name("JigSawParticles")));
        } else {
            ATH_CHECK(m_ParticleConstructor->LoadSubContainer(full_name("JigSawParticles")));
        }
        ATH_CHECK(m_JigSaws->Fill(m_ParticleConstructor->GetSubContainer(full_name("JigSawParticles"))));

        ATH_CHECK(fillDefaultValues());
        if (taus->empty()) return StatusCode::SUCCESS;
        const xAOD::IParticle* P1 = nullptr;
        const xAOD::IParticle* P2 = nullptr;
        // No leptons in the event. Nothing of particular interst
        if (muons->empty() && electrons->empty()) {
            return StatusCode::SUCCESS;
        } else if (muons->empty()) {
            P1 = electrons->at(0);
        } else {
            P1 = muons->at(0);
        }

        // There are more than one muon or electron in the event. This one is not of interest neither
        if (P2 == nullptr || muons->size() > 1 || electrons->size() > 1) {
            ATH_MSG_DEBUG("Skip event because it's not part of our topology");
            return StatusCode::SUCCESS;
        }
        m_lab_frame->ClearEvent();
        // Assign the missing-transverse eneergy
        xAOD::MissingET* met = m_met->GetValue();
        TVector3 met_TLV;
        met_TLV.SetPtEtaPhi(met->met(), 0, met->phi());
        m_InvisGroup->SetLabFrameThreeVector(met_TLV);

        // Define the taus in the event
        m_vis_Tau1->SetLabFrameFourVector(P1->p4());
        m_vis_Tau1->SetCharge(TypeToPdgId(P1));

        m_vis_Tau2->SetLabFrameFourVector(P2->p4());
        m_vis_Tau2->SetCharge(TypeToPdgId(P2));

        return StatusCode::SUCCESS;
    }

}  // namespace XAMPP
