#include <XAMPPstau/AnalysisUtils.h>
#include <XAMPPstau/StauSampleOverlap.h>
#include <xAODTruth/TruthParticleContainer.h>

namespace XAMPP {

    StauSampleOverlap::StauSampleOverlap(const std::string& myname) :
        AsgTool(myname),
        m_InfoHandle("EventInfoHandler"),
        m_systematics("SystematicsTool"),
        m_XAMPPInfo(nullptr),
        m_dec_gen_filt_met(nullptr),
        m_dec_gen_filt_ht(nullptr),
        m_dec_gen_weight_merge(nullptr) {
        declareProperty("SystematicsTool", m_systematics);
        m_InfoHandle.declarePropertyFor(this, "EventInfoHandler", "The XAMPP EventInfo handle");
    }

    StatusCode StauSampleOverlap::initialize() {
        ATH_CHECK(m_InfoHandle.retrieve());
        ATH_CHECK(m_systematics.retrieve());
        m_XAMPPInfo = dynamic_cast<XAMPP::EventInfo*>(m_InfoHandle.operator->());
        if (m_systematics->isData()) return StatusCode::SUCCESS;
        ATH_CHECK(m_XAMPPInfo->NewCommonEventVariable<double>("MCmergeWeight"));
        ATH_CHECK(m_XAMPPInfo->NewCommonEventVariable<float>("GenFiltMET"));
        ATH_CHECK(m_XAMPPInfo->NewCommonEventVariable<float>("GenFiltHT"));

        m_dec_gen_filt_met = m_XAMPPInfo->GetVariableStorage<float>("GenFiltMET");
        m_dec_gen_filt_ht = m_XAMPPInfo->GetVariableStorage<float>("GenFiltHT");
        m_dec_gen_weight_merge = m_XAMPPInfo->GetVariableStorage<double>("MCmergeWeight");

        return StatusCode::SUCCESS;
    }
    bool StauSampleOverlap::AcceptEvent() const {
        if (m_systematics->isData()) return true;
        int dsid = m_XAMPPInfo->mcChannelNumber();

        //   345935.PhPy8EG_A14_ttbarMET100_200_hdamp258p75_nonallhad

        //   407342.PhPy8EG_A14_ttbarHT1k5_hdamp258p75_nonallhad
        //   407343.PhPy8EG_A14_ttbarHT1k_1k5_hdamp258p75_nonallhad
        //   407344.PhPy8EG_A14_ttbarHT6c_1k_hdamp258p75_nonallhad

        //   407345.PhPy8EG_A14_ttbarMET200_300_hdamp258p75_nonallhad
        //   407346.PhPy8EG_A14_ttbarMET300_400_hdamp258p75_nonallhad
        //   407347.PhPy8EG_A14_ttbarMET400_hdamp258p75_nonallhad

        static const std::vector<int> DSID_ttbarsliced{410470, 345935, 407345, 407346, 407347, 407342, 407343, 407344};
        static const float PowHegPy_ttbar_min_HT = 600.e3;
        static const float PowHegPy_ttbar_min_MET = 100.e3;

        double W = 1.;
        if (std::find_if(DSID_ttbarsliced.begin(), DSID_ttbarsliced.end(), [&dsid](const int ele) { return ele == dsid; }) !=
            DSID_ttbarsliced.end()) {
            // Only the standard sample is available
            if (m_dec_gen_filt_met->GetValue() <= PowHegPy_ttbar_min_MET && m_dec_gen_filt_ht->GetValue() <= PowHegPy_ttbar_min_HT) {
                W = 1.;
            }
            // phase space where all the HT samples as well as the MET sliced samples contribute
            else if (m_dec_gen_filt_met->GetValue() > PowHegPy_ttbar_min_MET && m_dec_gen_filt_ht->GetValue() > PowHegPy_ttbar_min_HT) {
                W = 1. / 3.;
            }
            // Overlap between the bulk and one of the slices
            else {
                W = 0.5;
            }
        }
        //     361100.PowhegPythia8EvtGen_AZNLOCTEQ6L1_Wplusenu
        //     361101.PowhegPythia8EvtGen_AZNLOCTEQ6L1_Wplusmunu
        //     361102.PowhegPythia8EvtGen_AZNLOCTEQ6L1_Wplustaunu
        //     361103.PowhegPythia8EvtGen_AZNLOCTEQ6L1_Wminusenu
        //     361104.PowhegPythia8EvtGen_AZNLOCTEQ6L1_Wminusmunu
        //     361105.PowhegPythia8EvtGen_AZNLOCTEQ6L1_Wminustaunu
        //     Off shell w-jets samples with an invariant mass of at least 120 GeV with dsid 301060 - 301178

        else if ((dsid >= 361100 && dsid <= 361105) || (dsid >= 361106 && dsid <= 361108)) {
            const xAOD::TruthParticleContainer* truth_boson = nullptr;
            if (!evtStore()->retrieve(truth_boson, "TruthBoson").isSuccess()) return false;
            for (const auto primary : *truth_boson) {
                if (primary->m() >= 120.e3) {
                    W = 0.5;
                    break;
                }
            }
        }
        // Intrinsic overlap between the bulk samples and the off shell W boson samples
        else if (dsid >= 301060 && dsid <= 301178) {
            W = 0.5;
        }
        // Off shell DY->leptonlepton samples with an invariant mass of at least 120 GeV
        else if (dsid >= 301000 && dsid <= 301058) {
            W = 0.5;
        }

        return m_dec_gen_weight_merge->ConstStore(W).isSuccess();
    }

}  // namespace XAMPP
