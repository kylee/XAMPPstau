#include <XAMPPbase/AnalysisUtils.h>
#include <XAMPPstau/StauHadHadAnalysisConfig.h>

namespace XAMPP {
    StauHadHadAnalysisConfig::StauHadHadAnalysisConfig(std::string Analysis) : AnalysisConfig(Analysis) {}

    StatusCode StauHadHadAnalysisConfig::initializeCustomCuts() {
        //########################################################################
        //          Had-had stream
        //          Cut: (>= 2 taus && tau1Pt > 35 && tau2Pt > 25) ||
        //          (>= 1 tau && >= 1 ele && ele1Pt > 24) ||
        //          (>= 1 tau && >= 1 muon && muon1Pt > 20)
        //########################################################################
        CutFlow HadHad("tautau");

        Cut* Least1BaselineTauCut = NewSkimmingCut("N_{#tau}^{baseline}>=1", Cut::CutType::CutInt);
        if (!Least1BaselineTauCut->initialize("n_BaseTau", ">=1")) return StatusCode::FAILURE;
        HadHad.push_back(Least1BaselineTauCut);

        Cut* Least1BaselineEle = NewSkimmingCut("N_{#ele}^{baseline} >= 1", Cut::CutType::CutInt);
        if (!Least1BaselineEle->initialize("n_BaseElec", ">0")) return StatusCode::FAILURE;

        Cut* EleCaseBaselineElePtCut = NewSkimmingCut("p_{T} (#ele^{0}) > 24 GeV", Cut::CutType::CutPartFloat);
        if (!EleCaseBaselineElePtCut->initialize("electrons pt[0]", ">=24000")) return StatusCode::FAILURE;

        auto EleCaseCombCut = Least1BaselineEle->combine(EleCaseBaselineElePtCut, Cut::AND);

        Cut* Least2BaselineTauCut = NewSkimmingCut("N_{#tau}^{baseline}>=2", Cut::CutType::CutInt);
        if (!Least2BaselineTauCut->initialize("n_BaseTau", ">=2")) return StatusCode::FAILURE;

        Cut* TwoTauCaseTau1PtCut = NewSkimmingCut("p_{T} (#tau^{0}) > 35 GeV", Cut::CutType::CutPartFloat);
        if (!TwoTauCaseTau1PtCut->initialize("taus pt[0]", ">=35000")) return StatusCode::FAILURE;

        Cut* TwoTauCaseTau2PtCut = NewSkimmingCut("p_{T} (#tau^{1}) > 25 GeV", Cut::CutType::CutPartFloat);
        if (!TwoTauCaseTau2PtCut->initialize("taus pt[1]", ">=25000")) return StatusCode::FAILURE;

        Cut* TwoTauCaseMETCut = NewSkimmingCut("MetTST > 50 GeV", Cut::CutType::CutXAMPPmet);
        if (!TwoTauCaseMETCut->initialize("MetTST", ">=50000")) return StatusCode::FAILURE;

        Cut* TwoTauCaseTau1PtAsymCut = NewSkimmingCut("p_{T} (#tau^{0}) > 80 GeV", Cut::CutType::CutPartFloat);
        if (!TwoTauCaseTau1PtAsymCut->initialize("taus pt[0]", ">=80000")) return StatusCode::FAILURE;

        Cut* TwoTauCaseTau2PtAsymCut = NewSkimmingCut("p_{T} (#tau^{1}) > 50 GeV", Cut::CutType::CutPartFloat);
        if (!TwoTauCaseTau2PtAsymCut->initialize("taus pt[1]", ">=50000")) return StatusCode::FAILURE;

        auto TwoTauCaseCombCutDitauMET = Least2BaselineTauCut->combine(TwoTauCaseTau1PtCut, Cut::AND)
                                             ->combine(TwoTauCaseTau2PtCut, Cut::AND)
                                             ->combine(TwoTauCaseMETCut, Cut::AND);
        auto TwoTauCaseCombCutAsym =
            Least2BaselineTauCut->combine(TwoTauCaseTau1PtAsymCut, Cut::AND)->combine(TwoTauCaseTau2PtAsymCut, Cut::AND);

        Cut* Least1BaselineMuon = NewSkimmingCut("N_{#mu}^{baseline} >= 1", Cut::CutType::CutInt);
        if (!Least1BaselineMuon->initialize("n_BaseMuon", ">0")) return StatusCode::FAILURE;

        Cut* MuonCaseBaselineMuonPtCut = NewSkimmingCut("p_{T} (#muon^{0}) > 20 GeV", Cut::CutType::CutPartFloat);
        if (!MuonCaseBaselineMuonPtCut->initialize("muons pt[0]", ">=20000")) return StatusCode::FAILURE;

        auto MuonCaseCombCut = Least1BaselineMuon->combine(MuonCaseBaselineMuonPtCut, Cut::AND);

        HadHad.push_back(TwoTauCaseCombCutDitauMET->combine(TwoTauCaseCombCutAsym, Cut::OR)
                             ->combine(EleCaseCombCut, Cut::OR)
                             ->combine(MuonCaseCombCut, Cut::OR));

        Cut* Least1SignalTauCut = NewCutFlowCut("N_{#tau}^{signal} >= 1", Cut::CutType::CutInt);
        if (!Least1SignalTauCut->initialize("n_SignalTau", ">=1")) return StatusCode::FAILURE;
        HadHad.push_back(Least1SignalTauCut);

        Cut* Exact2SignalTauCut = NewCutFlowCut("N_{#tau}^{signal} == 2", Cut::CutType::CutInt);
        if (!Exact2SignalTauCut->initialize("n_SignalTau", ">=2")) return StatusCode::FAILURE;
        HadHad.push_back(Exact2SignalTauCut);

        Cut* OsTauTau = NewCutFlowCut("#tau^{+}#tau^{-}", Cut::CutType::CutChar);
        if (!OsTauTau->initialize("OS_TauTau", ">=1")) return StatusCode::FAILURE;
        HadHad.push_back(OsTauTau);

        Cut* TrigCut = NewCutFlowCut("TrigMatching", Cut::CutType::CutChar);
        if (!TrigCut->initialize("TrigMatching", "=1")) return StatusCode::FAILURE;
        HadHad.push_back(TrigCut);

        // Veto Additional leptons ? -- let's leave it to be optimized
        Cut* BaselineElecVeto = NewCutFlowCut("N_{e}^{baseline} = 0", Cut::CutType::CutInt);
        if (!BaselineElecVeto->initialize("n_BaseElec", "=0")) return StatusCode::FAILURE;

        Cut* BaselineMuonVeto = NewCutFlowCut("N_{#mu}^{baseline} = 0", Cut::CutType::CutInt);
        if (!BaselineMuonVeto->initialize("n_BaseMuon", "=0")) return StatusCode::FAILURE;

        HadHad.push_back(BaselineMuonVeto->combine(BaselineElecVeto, Cut::AND));

        Cut* BVeto = NewCutFlowCut("B-veto", Cut::CutType::CutInt);
        if (!BVeto->initialize("n_BJets", "==0")) return StatusCode::FAILURE;
        HadHad.push_back(BVeto);

        ATH_CHECK(AddToCutFlows(HadHad));

        return StatusCode::SUCCESS;
    }
}  // namespace XAMPP
