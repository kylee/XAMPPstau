Extracting MetaData information...
Having found CutFlow for MC, printing...
Printing raw events
CutFlowHisto             DSID_364190_CutFlow
Systematic variation     Nominal   
############################################
Cut                      Yields    
############################################
Initial                  124382 ± 352.68
PassGRL                  124382 ± 352.68
passLArTile              124382 ± 352.68
Trigger                  83257  ± 288.54
HasVtx                   83257  ± 288.54
BadJet                   82990  ± 288.08
CosmicMuon               82900  ± 287.92
BadMuon                  82815  ± 287.78
TrigMatching             51329  ± 226.56
N_{e}^{baseline} #geq 1  12720  ± 112.78
N_{e}^{loose} = 0        10523  ± 102.58
N_{#mu}^{baseline} = 0   10485  ± 102.40
N_{#tau}^{baseline}>=1   417    ± 20.42
E_{T}^{miss} > 15 GeV    391    ± 19.77
N_{#tau}^{signal}==1     81     ± 9.00
Final                    81     ± 9.00
############################################
Printing raw events
CutFlowHisto             DSID_364191_CutFlow
Systematic variation     Nominal   
############################################
Cut                      Yields    
############################################
Initial                  120899 ± 347.71
PassGRL                  120899 ± 347.71
passLArTile              120899 ± 347.71
Trigger                  81585  ± 285.63
HasVtx                   81585  ± 285.63
BadJet                   81303  ± 285.14
CosmicMuon               81125  ± 284.82
BadMuon                  81012  ± 284.63
TrigMatching             48770  ± 220.84
N_{e}^{baseline} #geq 1  11679  ± 108.07
N_{e}^{loose} = 0        9590   ± 97.93
N_{#mu}^{baseline} = 0   9476   ± 97.34
N_{#tau}^{baseline}>=1   407    ± 20.17
E_{T}^{miss} > 15 GeV    384    ± 19.60
N_{#tau}^{signal}==1     71     ± 8.43
Final                    71     ± 8.43
############################################
Printing raw events
CutFlowHisto             DSID_364192_CutFlow
Systematic variation     Nominal   
############################################
Cut                      Yields    
############################################
Initial                  115228 ± 339.45
PassGRL                  115228 ± 339.45
passLArTile              115228 ± 339.45
Trigger                  78076  ± 279.42
HasVtx                   78076  ± 279.42
BadJet                   77816  ± 278.96
CosmicMuon               76932  ± 277.37
BadMuon                  76740  ± 277.02
TrigMatching             46442  ± 215.50
N_{e}^{baseline} #geq 1  12246  ± 110.66
N_{e}^{loose} = 0        9413   ± 97.02
N_{#mu}^{baseline} = 0   9130   ± 95.55
N_{#tau}^{baseline}>=1   367    ± 19.16
E_{T}^{miss} > 15 GeV    340    ± 18.44
N_{#tau}^{signal}==1     73     ± 8.54
Final                    73     ± 8.54
############################################
