#!/env/python
from ClusterSubmission.Utils import ReadListFromFile, WriteList, ClearFromDuplicates, ResolvePath
from ClusterSubmission.AMIDataBase import getAMIDataBase
from XAMPPbase.CreateMergedNTUP_PILEUP import GetPRW_datasetID
import time

if __name__ == '__main__':

    DSIDS = [GetPRW_datasetID(f) for f in ReadListFromFile("XAMPPstau/SampleLists/mc16_13TeV/mc16_EVNT.txt")]
    #   getAMIDataBase().getMCDataSets(channels=DSIDS, campaign="mc16_13TeV", derivations=["EVNT"])
    getAMIDataBase().getMCDataSets(channels=DSIDS, campaign="mc15_13TeV", derivations=["DAOD_TRUTH3"])
    time.sleep(10)
    getAMIDataBase().getMCDataSets(channels=DSIDS, campaign="mc16_13TeV", derivations=["DAOD_TRUTH3"])

    out_evnt_ds = []
    out_truth_ds = []
    found = []
    for ds in DSIDS:
        for c in [15, 16]:
            ami_mc = getAMIDataBase().getMCchannel(ds, campaign="mc%d_13TeV" % (c))
            if not ami_mc:
                print "Whaat? %d" % (ds)
                continue
        #  ds_name = "%s.%d.%s.merge.EVNT.%s" % (ami_mc.campaign(), ds, ami_mc.name(), ami_mc.tags(data_type="EVNT")[0])
        #  out_evnt_ds += [ds_name]
            try:
                ds_truth_name = "%s.%d.%s.deriv.DAOD_TRUTH3.%s" % (ami_mc.campaign(), ds, ami_mc.name(),
                                                                   ami_mc.tags(data_type="DAOD_TRUTH3")[0])

                out_truth_ds += [ds_truth_name]
                found += [ds]
                break
            except:
                print "WARNING: No truth is available for %s" % (ami_mc.name())
#    WriteList(out_evnt_ds, ResolvePath("XAMPPstau/SampleLists/mc16_13TeV/mc16_EVNT.txt"))

    WriteList(out_truth_ds, ResolvePath("XAMPPstau/SampleLists/mc16_13TeV/mc16_TRUTH3.txt"))
    for ds in DSIDS:
        if ds in found: continue
        print "WARNING: Nothing could be found for %s" % (getAMIDataBase().getMCchannel(ds).name())
