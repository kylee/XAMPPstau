#ifndef StauMT2Module_H
#define StauMT2Module_H

#include <XAMPPstau/StauAnalysisModule.h>

namespace XAMPP {
    class StauMT2Module : public StauAnalysisModule {
    public:
        // Create a proper constructor for Athena
        ASG_TOOL_CLASS(StauMT2Module, XAMPP::IAnalysisModule)
        //
        StauMT2Module(const std::string& myname);
        virtual StatusCode bookVariables();
        virtual StatusCode fill();

        virtual ~StauMT2Module();

    private:
        XAMPP::Storage<float>* m_dec_MT2_min;
        XAMPP::Storage<float>* m_dec_MT2_max;
    };
}  // namespace XAMPP
#endif
