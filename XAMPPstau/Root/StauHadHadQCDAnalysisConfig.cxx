#include <XAMPPbase/AnalysisUtils.h>
#include <XAMPPstau/StauHadHadQCDAnalysisConfig.h>

namespace XAMPP {
    StauHadHadQCDAnalysisConfig::StauHadHadQCDAnalysisConfig(std::string Analysis) : AnalysisConfig(Analysis) {}

    StatusCode StauHadHadQCDAnalysisConfig::initializeCustomCuts() {
        //########################################################################
        //          Had-had QCD stream
        //########################################################################
        CutFlow HadHadQCD("tautau");

        Cut* Least1BaselineTauCut = NewSkimmingCut("N_{#tau}^{baseline}>=2", Cut::CutType::CutInt);
        if (!Least1BaselineTauCut->initialize("n_BaseTau", ">=2")) return StatusCode::FAILURE;
        HadHadQCD.push_back(Least1BaselineTauCut);

        Cut* Exact2SignalTauCut = NewSkimmingCut("N_{#tau}^{signal} < 2", Cut::CutType::CutInt);
        if (!Exact2SignalTauCut->initialize("n_SignalTau", "<2")) return StatusCode::FAILURE;
        HadHadQCD.push_back(Exact2SignalTauCut);

        Cut* BVeto = NewCutFlowCut("B-veto", Cut::CutType::CutInt);
        if (!BVeto->initialize("n_BJets", "==0")) return StatusCode::FAILURE;
        HadHadQCD.push_back(BVeto);

        ATH_CHECK(AddToCutFlows(HadHadQCD));

        return StatusCode::SUCCESS;
    }
}  // namespace XAMPP
