#!/bin/bash

# definition of folder for storing test results
export SGE_BASEFOLDER="${CI_PROJECT_DIR}/TEST_FOLDER/"
export JOB_MAIL="no_mail"
### Avoid twice setting up the atlas software
export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase    
mkdir -p ${SGE_BASEFOLDER}
#############################################
## Write the input config file              #
#############################################
echo "Input ${CI_PROJECT_DIR}/AnalysisOutput.root" > ${SGE_BASEFOLDER}/CI_Test.conf
### Thus far only the GGM sample is defined
echo "NoSUSYpidWeight" >> ${SGE_BASEFOLDER}/CI_Test.conf
### Check for the histo config
if [ ! -f "${TREE_CFG}" ];then
  echo "ERROR: Tree making config ${TREE_CFG} does not exist"
  exit 1
fi

### get the current date
DATE=`date --iso`
#### Execute the CI test 
python XAMPPplotting/python/SubmitToBatch.py -I ${SGE_BASEFOLDER}/CI_Test.conf  -T ${TREE_CFG} --engine LOCAL --jobName CI_Test --maxCurrentJobs 1
###
if [ $? -ne 0 ]; then
 echo "Execution of XAMPPplotting failed"
 exit 1
fi
### Create a log archive to spot what's gone wrong
tar -czf ${CI_PROJECT_DIR}/Logs.tar.gz ${SGE_BASEFOLDER}/LOGS/${DATE}/CI_Test/

### Finally copy the file to the CI_PROJECT_DIR
cp ${SGE_BASEFOLDER}/OUTPUT/${DATE}/CI_Test/CI_Test.root ${CI_PROJECT_DIR}
