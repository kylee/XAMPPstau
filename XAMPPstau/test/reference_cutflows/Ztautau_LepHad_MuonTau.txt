Extracting MetaData information...
Having found CutFlow for MC, printing...
Printing raw events
CutFlowHisto               DSID_364131_CutFlow
Systematic variation       Nominal   
##############################################
Cut                        Yields    
##############################################
Initial                    175185 ± 418.55
PassGRL                    175185 ± 418.55
passLArTile                175185 ± 418.55
Trigger                    145264 ± 381.14
HasVtx                     145264 ± 381.14
BadJet                     144565 ± 380.22
CosmicMuon                 143430 ± 378.72
BadMuon                    143140 ± 378.34
TrigMatching               87514  ± 295.83
N_{#mu}^{baseline} #geq 1  15851  ± 125.90
N_{#mu}^{loose} = 0        7863   ± 88.67
N_{e}^{baseline} = 0       6544   ± 80.89
N_{#tau}^{baseline}>=1     3871   ± 62.22
E_{T}^{miss} > 15 GeV      3280   ± 57.27
N_{#tau}^{signal}==1       2309   ± 48.05
Final                      2309   ± 48.05
##############################################
Printing raw events
CutFlowHisto               DSID_364132_CutFlow
Systematic variation       Nominal   
##############################################
Cut                        Yields    
##############################################
Initial                    65222 ± 255.39
PassGRL                    65222 ± 255.39
passLArTile                65222 ± 255.39
Trigger                    56046 ± 236.74
HasVtx                     56046 ± 236.74
BadJet                     55766 ± 236.15
CosmicMuon                 55240 ± 235.03
BadMuon                    55084 ± 234.70
TrigMatching               33581 ± 183.25
N_{#mu}^{baseline} #geq 1  6308  ± 79.42
N_{#mu}^{loose} = 0        3020  ± 54.95
N_{e}^{baseline} = 0       2541  ± 50.41
N_{#tau}^{baseline}>=1     1578  ± 39.72
E_{T}^{miss} > 15 GeV      1338  ± 36.58
N_{#tau}^{signal}==1       915   ± 30.25
Final                      915   ± 30.25
##############################################
Printing raw events
CutFlowHisto               DSID_364133_CutFlow
Systematic variation       Nominal   
##############################################
Cut                        Yields    
##############################################
Initial                    108649 ± 329.62
PassGRL                    108649 ± 329.62
passLArTile                108649 ± 329.62
Trigger                    94813  ± 307.92
HasVtx                     94813  ± 307.92
BadJet                     94451  ± 307.33
CosmicMuon                 92629  ± 304.35
BadMuon                    92327  ± 303.85
TrigMatching               58384  ± 241.63
N_{#mu}^{baseline} #geq 1  12317  ± 110.98
N_{#mu}^{loose} = 0        5589   ± 74.76
N_{e}^{baseline} = 0       4549   ± 67.45
N_{#tau}^{baseline}>=1     2574   ± 50.73
E_{T}^{miss} > 15 GeV      2135   ± 46.21
N_{#tau}^{signal}==1       1554   ± 39.42
Final                      1554   ± 39.42
##############################################
