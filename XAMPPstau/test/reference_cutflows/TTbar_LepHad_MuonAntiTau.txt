Extracting MetaData information...
Having found CutFlow for MC, printing...
Printing raw events
CutFlowHisto               DSID_410470_CutFlow
Systematic variation       Nominal   
##############################################
Cut                        Yields    
##############################################
Initial                    114802 ± 338.82
PassGRL                    114802 ± 338.82
passLArTile                114802 ± 338.82
Trigger                    96460  ± 310.58
HasVtx                     96460  ± 310.58
BadJet                     96129  ± 310.05
CosmicMuon                 94718  ± 307.76
BadMuon                    94512  ± 307.43
TrigMatching               70883  ± 266.24
N_{#mu}^{baseline} #geq 1  22213  ± 149.04
N_{#mu}^{loose} = 0        18817  ± 137.18
N_{e}^{baseline} = 0       11844  ± 108.83
N_{#tau}^{baseline}>=1     3356   ± 57.93
E_{T}^{miss} > 15 GeV      3233   ± 56.86
N_{#tau}^{signal}==0       1956   ± 44.23
N_{bjet} == 0              204    ± 14.28
Final                      204    ± 14.28
##############################################
