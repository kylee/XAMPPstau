#include <IsolationSelection/IsolationWP.h>
#include <SUSYTools/SUSYObjDef_xAOD.h>
#include <XAMPPbase/AnalysisUtils.h>
#include <XAMPPbase/EventInfo.h>
#include <XAMPPbase/IElectronSelector.h>
#include <XAMPPbase/IJetSelector.h>
#include <XAMPPbase/IMetSelector.h>
#include <XAMPPbase/IMuonSelector.h>
#include <XAMPPbase/IPhotonSelector.h>
#include <XAMPPbase/ITriggerTool.h>
#include <XAMPPbase/ITruthSelector.h>
#include <XAMPPbase/ReconstructedParticles.h>
#include <XAMPPbase/SUSYSystematics.h>
#include <XAMPPbase/SUSYTauSelector.h>
#include <XAMPPbase/ToolHandleSystematics.h>
#include <XAMPPstau/AnalysisUtils.h>
#include <XAMPPstau/StauAnalysisHelper.h>
#include <fstream>
#include <iostream>
#include <sstream>

namespace XAMPP {

    StauAnalysisHelper::StauAnalysisHelper(const std::string& myname) :
        SUSYAnalysisHelper(myname),
        m_NumTauCandidate_OR(1),
        m_mergeSamples(false),
        m_doTauPromotion(false),
        m_doTauBJetOR(false),
        m_doTrackMet(false),
        m_smp_OR_removal(""),
        m_isoWPs(),
        m_isoTester_Elecs(),
        m_isoTester_Muons() {
        // Specify the maximum number of tau candidates for the overlap removal with jets
        declareProperty("TauCandidatesForOR", m_NumTauCandidate_OR);
        declareProperty("doTauPromotion", m_doTauPromotion);
        declareProperty("DoTrackMet", m_doTrackMet);
        declareProperty("DoSampleMerging", m_mergeSamples);

        // Give the priority over b-jets rather than taus
        declareProperty("DoTauBJetOR", m_doTauBJetOR);

        declareProperty("IsolationWPTools", m_isoWPs);
        m_smp_OR_removal.declarePropertyFor(this, "SampleOverlapRemoval");
    }
    StauAnalysisHelper::~StauAnalysisHelper() {}

    bool StauAnalysisHelper::AcceptEvent() {
        if (!SUSYAnalysisHelper::AcceptEvent()) return false;
        if (isData() || !m_mergeSamples) return true;
        return m_smp_OR_removal->AcceptEvent();
    }
    StatusCode StauAnalysisHelper::initializeAnalysisTools() {
        ATH_CHECK(SUSYAnalysisHelper::initializeAnalysisTools());
        ATH_CHECK(m_isoWPs.retrieve());
        return StatusCode::SUCCESS;
    }
    StatusCode StauAnalysisHelper::RemoveOverlap() {
        ATH_MSG_DEBUG("OverlapRemoval...");
        if (m_systematics->AffectsOnlyMET(m_systematics->GetCurrent())) return StatusCode::SUCCESS;

        // The order by the OR tool is given here
        // https://gitlab.cern.ch/atlas/athena/blob/21.2/PhysicsAnalysis/AnalysisCommon/AssociationUtils/Root/OverlapRemovalTool.cxx#L106
        ATH_CHECK(m_susytools->OverlapRemoval(m_electron_selection->GetPreElectrons(),  // Electrons
                                              m_muon_selection->GetPreMuons(),          // Muons
                                              m_jet_selection->GetPreJets(),            // Jets
                                              m_photon_selection->GetPrePhotons(),      // Photons (Disabled by default -> empty container)
                                              m_tau_selection->GetPreTaus()             // Taus
                                              ));

        /// The tau-jet OR is the last step of the overlap removal
        /// Instead of performing the OR twice with partial containers we're
        /// going to restore the jets by hand
        if (!m_tau_selection->GetPreTaus()->empty() && (m_NumTauCandidate_OR > 0 || m_doTauBJetOR)) {
            unsigned int num_tau = 0;
            for (auto pre_tau : *m_tau_selection->GetPreTaus()) {
                if (!m_tau_selection->GetTauDecorations()->passBaseline(pre_tau)) continue;
                for (auto pre_jet : *m_jet_selection->GetPreJets()) {
                    /// Use eta instead of the rapidity
                    if (!Overlaps(pre_jet, pre_tau, 0.2, false)) continue;
                    if (m_NumTauCandidate_OR > 0 && (int)num_tau >= m_NumTauCandidate_OR) {
                        m_jet_selection->GetJetDecorations()->passBaseline.set(pre_jet, true);
                        /// Reject the tau then?
                    } else if (m_doTauBJetOR) {
                        if (m_jet_selection->GetJetDecorations()->isBJet_Loose(pre_jet) &&
                            !m_tau_selection->GetTauDecorations()->passSignal(pre_tau)) {
                            m_jet_selection->GetJetDecorations()->passBaseline.set(pre_jet, true);
                            m_tau_selection->GetTauDecorations()->passBaseline.set(pre_tau, false);
                            --num_tau;
                        }
                    }
                }
                ++num_tau;
            }
        }

        if (!m_isoTester_Elecs.empty()) {
            for (auto ele : *m_electron_selection->GetPreElectrons()) {
                for (const auto& tester : m_isoTester_Elecs) { (*tester.particleDecoration)(*ele) = tester.isoTool->accept(*ele); }
            }
        }
        if (!m_isoTester_Muons.empty()) {
            for (auto muo : *m_muon_selection->GetPreMuons()) {
                for (const auto& tester : m_isoTester_Muons) { (*tester.particleDecoration)(*muo) = tester.isoTool->accept(*muo); }
            }
        }
        return StatusCode::SUCCESS;
    }
    StatusCode StauAnalysisHelper::initializeLightLeptonTree() {
        ATH_CHECK(m_XAMPPInfo->BookParticleStorage("muons"));
        ATH_CHECK(m_XAMPPInfo->BookParticleStorage("electrons"));

        ParticleStorage* MuonStore = m_XAMPPInfo->GetParticleStorage("muons");
        ParticleStorage* ElectronStore = m_XAMPPInfo->GetParticleStorage("electrons");
        StringVector FloatVars{"charge", "z0sinTheta", "d0sig", "MT"};
        StringVector CharVars{"isol", "signal"};
        StringVector IntVars;
        if (!isData()) {
            IntVars.push_back("truthType");
            IntVars.push_back("truthOrigin");
            IntVars.push_back("IFFClassType");
        }
        ATH_CHECK(MuonStore->SaveVariable<float>(FloatVars));
        ATH_CHECK(ElectronStore->SaveVariable<float>(FloatVars));

        ATH_CHECK(MuonStore->SaveVariable<char>(CharVars));
        ATH_CHECK(ElectronStore->SaveVariable<char>(CharVars));

        ATH_CHECK(MuonStore->SaveVariable<int>(IntVars));
        ATH_CHECK(ElectronStore->SaveVariable<int>(IntVars));

        for (const auto& iso_tool : m_isoWPs) {
            for (const auto& WP : iso_tool->getMuonWPs()) {
                ATH_MSG_INFO("Muon working point " << WP->name());
                m_isoTester_Muons.push_back(IsolationTester(WP->name(), iso_tool));
                ATH_CHECK(MuonStore->SaveVariable<char>("isol_" + WP->name()));
            }
            for (const auto& WP : iso_tool->getElectronWPs()) {
                ATH_MSG_INFO("Electrond working point " << WP->name());
                m_isoTester_Elecs.push_back(IsolationTester(WP->name(), iso_tool));
                ATH_CHECK(ElectronStore->SaveVariable<char>("isol_" + WP->name()));
            }
        }
        /// Systematic groups are designed to reduce the file size of
        /// the XAMPP output. Variables which are not affected by a particular systematic are stored in
        /// a separate tree
        if (buildCommonTree()) {
            ATH_CHECK(m_XAMPPInfo->createSystematicGroup("ElectronGroup", SelectionObject::Electron));
            ATH_CHECK(ElectronStore->setSystematicGroup("ElectronGroup"));

            ATH_CHECK(m_XAMPPInfo->createSystematicGroup("MuonGroup", SelectionObject::Muon));
            ATH_CHECK(MuonStore->setSystematicGroup("MuonGroup"));
            ATH_CHECK(ElectronStore->SaveVariable<char>("passOR"));
            ATH_CHECK(MuonStore->SaveVariable<char>("passOR"));

            ElectronStore->pipeVariableToAllTrees(StringVector{"passOR", "MT"});
            MuonStore->pipeVariableToAllTrees(StringVector{"passOR", "MT"});
        }

        return StatusCode::SUCCESS;
    }
    StatusCode StauAnalysisHelper::initializeJetTree() {
        ATH_CHECK(m_XAMPPInfo->BookParticleStorage("jets", true));
        ParticleStorage* JetStore = m_XAMPPInfo->GetParticleStorage("jets");
        StringVector FloatVars{"Jvt"};
        StringVector IntVars{"NTrks"};
        StringVector CharVars{"signal", "bjet", "isBadTight"};
        StringVector DoubleVars{"BTagScore"};
        ATH_CHECK(JetStore->SaveVariable<int>(IntVars));
        ATH_CHECK(JetStore->SaveVariable<float>(FloatVars));
        ATH_CHECK(JetStore->SaveVariable<char>(CharVars));
        ATH_CHECK(JetStore->SaveVariable<double>(DoubleVars));
        if (buildCommonTree()) {
            ATH_CHECK(m_XAMPPInfo->createSystematicGroup("JetGroup", SelectionObject::Jet));
            ATH_CHECK(JetStore->setSystematicGroup("JetGroup"));
            ATH_CHECK(JetStore->SaveVariable<char>("passOR"));
            JetStore->pipeVariableToAllTrees("passOR");
        }
        return StatusCode::SUCCESS;
    }
    StatusCode StauAnalysisHelper::initializeDiLeptonTree() {
        ATH_CHECK(m_XAMPPInfo->BookParticleStorage("dilepton", true));
        ParticleStorage* DiLepStore = m_XAMPPInfo->GetParticleStorage("dilepton");
        StringVector FloatVars{"charge"};
        StringVector IntVars{"pdgId"};
        StringVector CharVars;
        ATH_CHECK(DiLepStore->SaveVariable<int>(IntVars));
        ATH_CHECK(DiLepStore->SaveVariable<float>(FloatVars));
        ATH_CHECK(DiLepStore->SaveVariable<char>(CharVars));

        return StatusCode::SUCCESS;
    }
    StatusCode StauAnalysisHelper::initializeTauTree() {
        ATH_CHECK(m_XAMPPInfo->BookParticleStorage("taus"));
        ParticleStorage* TauStore = m_XAMPPInfo->GetParticleStorage("taus");
        StringVector FloatVars{"MT", "charge", "Width",      "BDTEleScore", "BDTEleScoreSigTrans", "RNNJetScore", "RNNJetScoreSigTrans",
                               "d0", "d0sig",  "z0sinTheta", "TrkJetWidth"};
        StringVector IntVars{"NTrks", "NTrksJet", "Quality"};
        StringVector CharVars{"signalID"};
        StringVector VecFloatVars{"trks_z0sinTheta", "trks_d0sig", "trks_d0"};
        if (!isData()) {
            IntVars.push_back("truthType");
            IntVars.push_back("truthOrigin");
            IntVars.push_back("PartonTruthLabelID");
            IntVars.push_back("ConeTruthLabelID");
            if (m_doTauPromotion) CharVars.push_back("isPromoted");
        }
        ATH_CHECK(TauStore->SaveVariable<int>(IntVars));
        ATH_CHECK(TauStore->SaveVariable<float>(FloatVars));
        for (auto& vec_var : VecFloatVars) { ATH_CHECK(TauStore->SaveVariable<std::vector<float>>(vec_var)); }
        ATH_CHECK(TauStore->SaveVariable<char>(CharVars));
        ATH_CHECK(m_triggers->SaveObjectMatching(TauStore, xAOD::Type::ObjectType::Tau));

        if (buildCommonTree()) {
            ATH_CHECK(m_XAMPPInfo->createSystematicGroup("TauGroup", SelectionObject::Tau));
            ATH_CHECK(TauStore->setSystematicGroup("TauGroup"));
            ATH_CHECK(TauStore->SaveVariable<char>("passOR"));
            TauStore->pipeVariableToAllTrees(StringVector{"passOR", "isPromoted", "MT"});
        }

        return StatusCode::SUCCESS;
    }
    StatusCode StauAnalysisHelper::initializeModuleContainers() {
        StringVector stores{"signal_electrons", "signal_muons",   "candidate_taus", "signal_jets",
                            "signal_lightlep",  "signal_leptons", "signal_taus",    "baseline_taus"};
        for (const auto& S : stores) { ATH_CHECK(m_XAMPPInfo->BookParticleStorage(S, false, false, false)); }
        return StatusCode::SUCCESS;
    }
    StatusCode StauAnalysisHelper::initializeEventVariables() {
        if (m_mergeSamples) {
            if (!m_smp_OR_removal.isUserConfigured()) m_smp_OR_removal.setTypeAndName("XAMPP::StauSampleOverlap/MCSampleORremovalTool");
            ATH_CHECK(m_smp_OR_removal.retrieve());
        }
        ATH_CHECK(initializeLightLeptonTree());
        ATH_CHECK(initializeModuleContainers());

        ATH_CHECK(initializeDiLeptonTree());
        ATH_CHECK(initializeJetTree());
        ATH_CHECK(initializeTauTree());

        // Write out the particle multiplicity in the events

        ATH_CHECK(m_XAMPPInfo->NewEventVariable<int>("n_BaseElec", true));
        ATH_CHECK(m_XAMPPInfo->NewEventVariable<int>("n_BaseMuon", true));

        /// Difference between baseline and Loose objects
        ATH_CHECK(m_XAMPPInfo->NewEventVariable<int>("n_LooseElec", false));
        ATH_CHECK(m_XAMPPInfo->NewEventVariable<int>("n_LooseMuon", false));

        ATH_CHECK(m_XAMPPInfo->NewEventVariable<int>("n_SignalElec"));
        ATH_CHECK(m_XAMPPInfo->NewEventVariable<int>("n_SignalMuon"));

        ATH_CHECK(m_XAMPPInfo->NewEventVariable<int>("n_BaseTau"));
        ATH_CHECK(m_XAMPPInfo->NewEventVariable<int>("n_SignalTau"));

        ATH_CHECK(m_XAMPPInfo->NewEventVariable<int>("n_BaseJets"));
        ATH_CHECK(m_XAMPPInfo->NewEventVariable<int>("n_SignalJets"));
        ATH_CHECK(m_XAMPPInfo->NewEventVariable<int>("n_BJets"));

        ATH_CHECK(m_XAMPPInfo->NewEventVariable<char>("OS_TauEle"));
        ATH_CHECK(m_XAMPPInfo->NewEventVariable<char>("OS_TauMuo"));

        ATH_CHECK(m_XAMPPInfo->NewEventVariable<char>("OS_TauTau"));
        ATH_CHECK(m_XAMPPInfo->NewEventVariable<char>("OS_EleEle"));
        ATH_CHECK(m_XAMPPInfo->NewEventVariable<char>("OS_MuoMuo"));

        ATH_CHECK(m_XAMPPInfo->NewEventVariable<char>("OS_BaseTauEle"));
        ATH_CHECK(m_XAMPPInfo->NewEventVariable<char>("OS_BaseTauMuo"));

        ATH_CHECK(m_XAMPPInfo->NewEventVariable<float>("dPhiMetTrackMetTST", m_doTrackMet));

        if (!isData()) {
            if (!m_mergeSamples) {
                ATH_CHECK(m_XAMPPInfo->NewCommonEventVariable<float>("GenFiltMET"));
                ATH_CHECK(m_XAMPPInfo->NewCommonEventVariable<float>("GenFiltHT"));
            }
            ATH_CHECK(m_XAMPPInfo->NewCommonEventVariable<int>("LQ_ncl"));
        }

        return StatusCode::SUCCESS;
    }

    StatusCode StauAnalysisHelper::ComputeEventVariables() {
        static CharDecorator dec_isBadTight("isBadTight");
        for (const auto& jet : !buildCommonTree() ? *m_jet_selection->GetSignalJets() : *m_jet_selection->GetSignalNoORJets()) {
            bool isbad = false;
            if (jet->isAvailable<char>("DFCommonJets_jetClean_TightBad")) {
                isbad = jet->auxdataConst<char>("DFCommonJets_jetClean_TightBad") != 1;
            } else {
                // PFlow jets are currently not decorated by the DerivationFramework
                // https://gitlab.cern.ch/atlas/athena/-/blob/21.2/PhysicsAnalysis/JetMissingEtID/JetSelectorTools/Root/JetCleaningTool.cxx#L329
                std::vector<float> sumPtTrkvec;
                ATH_CHECK(jet->getAttribute(xAOD::JetAttribute::SumPtTrkPt500, sumPtTrkvec));
                float sumpttrk = 0;
                if (!sumPtTrkvec.empty()) sumpttrk = sumPtTrkvec[0];
                float FracSamplingMax = 0;
                ATH_CHECK(jet->getAttribute(xAOD::JetAttribute::FracSamplingMax, FracSamplingMax));
                if (FracSamplingMax == 0.0 ||
                    (FracSamplingMax > 0.0 && std::fabs(jet->eta()) < 2.4 && sumpttrk / jet->pt() / FracSamplingMax < 0.1))
                    isbad = true;
            }
            dec_isBadTight(*jet) = isbad;
        }

        //###############################################################
        //                  Save the objects to the trees
        //###############################################################
        // muons
        static XAMPP::ParticleStorage* MuonStore = m_XAMPPInfo->GetParticleStorage("muons");
        ATH_CHECK(MuonStore->Fill(buildCommonTree() ? m_muon_selection->GetSignalNoORMuons() : m_muon_selection->GetBaselineMuons()));
        // electrons
        static XAMPP::ParticleStorage* ElectronStore = m_XAMPPInfo->GetParticleStorage("electrons");
        ATH_CHECK(ElectronStore->Fill(buildCommonTree() ? m_electron_selection->GetSignalNoORElectrons()
                                                        : m_electron_selection->GetBaselineElectrons()));
        // jets
        static XAMPP::ParticleStorage* JetStore = m_XAMPPInfo->GetParticleStorage("jets");
        ATH_CHECK(JetStore->Fill(!buildCommonTree() ? m_jet_selection->GetSignalJets() : m_jet_selection->GetSignalNoORJets()));
        // taus
        static XAMPP::ParticleStorage* TauStore = m_XAMPPInfo->GetParticleStorage("taus");
        ATH_CHECK(TauStore->Fill(buildCommonTree() ? m_tau_selection->GetPreTaus() : m_tau_selection->GetBaselineTaus()));

        // Storages for the Analysis modules
        static XAMPP::ParticleStorage* Module_SignalMuonStore = m_XAMPPInfo->GetParticleStorage("signal_muons");
        ATH_CHECK(Module_SignalMuonStore->Fill(m_muon_selection->GetSignalMuons()));
        // electrons
        static XAMPP::ParticleStorage* Module_ElectronStore = m_XAMPPInfo->GetParticleStorage("signal_electrons");
        ATH_CHECK(Module_ElectronStore->Fill(m_electron_selection->GetSignalElectrons()));
        // jets
        static XAMPP::ParticleStorage* Module_JetStore = m_XAMPPInfo->GetParticleStorage("signal_jets");
        ATH_CHECK(Module_JetStore->Fill(m_jet_selection->GetSignalJets()));
        // taus
        static XAMPP::ParticleStorage* Module_TauStore = m_XAMPPInfo->GetParticleStorage("candidate_taus");
        ATH_CHECK(Module_TauStore->Fill(m_tau_selection->GetCustomTaus("TausAfterOR")));

        static XAMPP::ParticleStorage* Module_SigTauStore = m_XAMPPInfo->GetParticleStorage("signal_taus");
        ATH_CHECK(Module_SigTauStore->Fill(m_tau_selection->GetSignalTaus()));

        static XAMPP::ParticleStorage* Module_BaseTauStore = m_XAMPPInfo->GetParticleStorage("baseline_taus");
        ATH_CHECK(Module_BaseTauStore->Fill(m_tau_selection->GetBaselineTaus()));

        // Save the number of particles in the final ntuple
        static XAMPP::Storage<int>* dec_nBaseElec = m_XAMPPInfo->GetVariableStorage<int>("n_BaseElec");
        static XAMPP::Storage<int>* dec_nSignalElec = m_XAMPPInfo->GetVariableStorage<int>("n_SignalElec");
        static XAMPP::Storage<int>* dec_nLooseElec = m_XAMPPInfo->GetVariableStorage<int>("n_LooseElec");

        static XAMPP::Storage<int>* dec_nBaseMuon = m_XAMPPInfo->GetVariableStorage<int>("n_BaseMuon");
        static XAMPP::Storage<int>* dec_nSignalMuon = m_XAMPPInfo->GetVariableStorage<int>("n_SignalMuon");
        static XAMPP::Storage<int>* dec_nLooseMuon = m_XAMPPInfo->GetVariableStorage<int>("n_LooseMuon");

        static XAMPP::Storage<int>* dec_nBaseTau = m_XAMPPInfo->GetVariableStorage<int>("n_BaseTau");
        static XAMPP::Storage<int>* dec_nSignalTau = m_XAMPPInfo->GetVariableStorage<int>("n_SignalTau");

        static XAMPP::Storage<int>* dec_nBaseJets = m_XAMPPInfo->GetVariableStorage<int>("n_BaseJets");
        static XAMPP::Storage<int>* dec_nSignalJets = m_XAMPPInfo->GetVariableStorage<int>("n_SignalJets");
        static XAMPP::Storage<int>* dec_nBJets = m_XAMPPInfo->GetVariableStorage<int>("n_BJets");

        ATH_CHECK(dec_nBaseElec->Store(m_electron_selection->GetBaselineElectrons()->size()));
        ATH_CHECK(dec_nSignalElec->Store(m_electron_selection->GetSignalElectrons()->size()));
        ATH_CHECK(dec_nLooseElec->Store(dec_nBaseElec->GetValue() - dec_nSignalElec->GetValue()));

        ATH_CHECK(dec_nBaseMuon->Store(m_muon_selection->GetBaselineMuons()->size()));
        ATH_CHECK(dec_nSignalMuon->Store(m_muon_selection->GetSignalMuons()->size()));
        ATH_CHECK(dec_nLooseMuon->Store(dec_nBaseMuon->GetValue() - dec_nSignalMuon->GetValue()));

        ATH_CHECK(dec_nBaseTau->Store(m_tau_selection->GetBaselineTaus()->size()));
        ATH_CHECK(dec_nSignalTau->Store(m_tau_selection->GetSignalTaus()->size()));

        ATH_CHECK(dec_nBaseJets->Store(m_jet_selection->GetBaselineJets()->size()));
        ATH_CHECK(dec_nSignalJets->Store(m_jet_selection->GetSignalJets()->size()));
        ATH_CHECK(dec_nBJets->Store(m_jet_selection->GetBJets()->size()));

        static XAMPP::Storage<xAOD::MissingET*>* dec_MET = m_XAMPPInfo->GetVariableStorage<xAOD::MissingET*>("MetTST");
        // Construct the MT
        CalculateMt(buildCommonTree() ? m_electron_selection->GetSignalNoORElectrons() : m_electron_selection->GetBaselineElectrons(),
                    dec_MET);
        CalculateMt(buildCommonTree() ? m_muon_selection->GetSignalNoORMuons() : m_muon_selection->GetBaselineMuons(), dec_MET);
        CalculateMt(m_tau_selection->GetPreTaus(), dec_MET);

        // Create bulk light lepton container
        xAOD::IParticleContainer* LightLeptons(nullptr);
        ATH_CHECK(ViewElementsContainer("LightSignalLeptons", LightLeptons));
        CopyContainer(m_electron_selection->GetSignalElectrons(), LightLeptons);
        CopyContainer(m_muon_selection->GetSignalMuons(), LightLeptons);
        LightLeptons->sort(XAMPP::ptsorter);

        // Create bulk lepton container
        xAOD::IParticleContainer* Leptons(nullptr);
        ATH_CHECK(ViewElementsContainer("Bulk_SignalLeptons", Leptons));
        CopyContainer(LightLeptons, Leptons);
        CopyContainer(m_tau_selection->GetCustomTaus("TausAfterOR"), Leptons);
        Leptons->sort(XAMPP::ptsorter);

        // signal_leptons
        static XAMPP::ParticleStorage* Module_LeptonStore = m_XAMPPInfo->GetParticleStorage("signal_leptons");
        ATH_CHECK(Module_LeptonStore->Fill(Leptons));

        static XAMPP::ParticleStorage* Module_LightLeptonStore = m_XAMPPInfo->GetParticleStorage("signal_lightlep");
        ATH_CHECK(Module_LightLeptonStore->Fill(Leptons));

        // select objects
        const xAOD::TauJet* LeadBaseTau = nullptr;
        if (dec_nBaseTau->GetValue() > 0) LeadBaseTau = m_tau_selection->GetBaselineTaus()->at(0);

        const xAOD::TauJet* LeadSignalTau = nullptr;
        if (dec_nSignalTau->GetValue() > 0) LeadSignalTau = m_tau_selection->GetSignalTaus()->at(0);

        const xAOD::Electron* LeadSignalElec = nullptr;
        if (dec_nSignalElec->GetValue() > 0) LeadSignalElec = m_electron_selection->GetSignalElectrons()->at(0);

        const xAOD::Muon* LeadSignalMuon = nullptr;
        if (dec_nSignalMuon->GetValue() > 0) LeadSignalMuon = m_muon_selection->GetSignalMuons()->at(0);

        const xAOD::Electron* SubLeadSignalElec = nullptr;
        if (dec_nSignalElec->GetValue() > 1) SubLeadSignalElec = m_electron_selection->GetSignalElectrons()->at(1);

        const xAOD::Muon* SubLeadSignalMuon = nullptr;
        if (dec_nSignalMuon->GetValue() > 1) SubLeadSignalMuon = m_muon_selection->GetSignalMuons()->at(1);

        const xAOD::TauJet* SubLeadSignalTau = nullptr;
        if (dec_nSignalTau->GetValue() > 1) SubLeadSignalTau = m_tau_selection->GetSignalTaus()->at(1);

        // Find the opposite sign pairs
        static XAMPP::Storage<char>* dec_OSTauEle = m_XAMPPInfo->GetVariableStorage<char>("OS_TauEle");
        static XAMPP::Storage<char>* dec_OSTauMuo = m_XAMPPInfo->GetVariableStorage<char>("OS_TauMuo");
        static XAMPP::Storage<char>* dec_OSEleEle = m_XAMPPInfo->GetVariableStorage<char>("OS_EleEle");
        static XAMPP::Storage<char>* dec_OSMuoMuo = m_XAMPPInfo->GetVariableStorage<char>("OS_MuoMuo");
        static XAMPP::Storage<char>* dec_OSTauTau = m_XAMPPInfo->GetVariableStorage<char>("OS_TauTau");
        static XAMPP::Storage<char>* dec_OSBaseTauEle = m_XAMPPInfo->GetVariableStorage<char>("OS_BaseTauEle");
        static XAMPP::Storage<char>* dec_OSBaseTauMuo = m_XAMPPInfo->GetVariableStorage<char>("OS_BaseTauMuo");

        bool OSEleTau = (LeadSignalTau != nullptr && LeadSignalElec != nullptr && OppositeSign(LeadSignalElec, LeadSignalTau));
        bool OSMuoTau = (LeadSignalTau != nullptr && LeadSignalMuon != nullptr && OppositeSign(LeadSignalMuon, LeadSignalTau));
        bool OSMuoMuo = (LeadSignalMuon != nullptr && SubLeadSignalMuon != nullptr && OppositeSign(SubLeadSignalMuon, LeadSignalMuon));
        bool OSEleEle = (LeadSignalElec != nullptr && SubLeadSignalElec != nullptr && OppositeSign(SubLeadSignalElec, LeadSignalElec));
        bool OSTauTau = (LeadSignalTau != nullptr && SubLeadSignalTau != nullptr && OppositeSign(SubLeadSignalTau, LeadSignalTau));
        bool OSEleBaseTau = (LeadBaseTau != nullptr && LeadSignalElec != nullptr && OppositeSign(LeadSignalElec, LeadBaseTau));
        bool OSMuoBaseTau = (LeadBaseTau != nullptr && LeadSignalMuon != nullptr && OppositeSign(LeadSignalMuon, LeadBaseTau));

        ATH_CHECK(dec_OSTauEle->Store(OSEleTau));
        ATH_CHECK(dec_OSTauMuo->Store(OSMuoTau));
        ATH_CHECK(dec_OSEleEle->Store(OSMuoMuo));
        ATH_CHECK(dec_OSMuoMuo->Store(OSEleEle));
        ATH_CHECK(dec_OSTauTau->Store(OSTauTau));
        ATH_CHECK(dec_OSBaseTauEle->Store(OSEleBaseTau));
        ATH_CHECK(dec_OSBaseTauMuo->Store(OSMuoBaseTau));

        if (m_doTrackMet) {
            static XAMPP::Storage<XAMPPmet>* dec_MetTrack = m_XAMPPInfo->GetVariableStorage<XAMPPmet>("MetTrack");
            static XAMPP::Storage<float>* dec_dPhiMet = m_XAMPPInfo->GetVariableStorage<float>("dPhiMetTrackMetTST");
            float dPhi = XAMPP::ComputeDeltaPhi(dec_MET, dec_MetTrack);
            ATH_CHECK(dec_dPhiMet->Store(dPhi));
        }
        // Calculate the invariant momenta of the leptons themselves
        ATH_CHECK(m_ParticleConstructor->CreateSubContainer("DiLepCandidates"));
        ConstructInvariantMomenta(Leptons, m_ParticleConstructor,
                                  [this](const xAOD::IParticle* P) {
                                      if (P->type() == xAOD::Type::ObjectType::Electron) {
                                          return m_electron_selection->GetElectronDecorations()->passSignal(*P) &&
                                                 m_electron_selection->GetElectronDecorations()->passIsolation(*P);
                                      } else if (P->type() == xAOD::Type::ObjectType::Muon) {
                                          return m_muon_selection->GetMuonDecorations()->passSignal(*P) &&
                                                 m_muon_selection->GetMuonDecorations()->passIsolation(*P);
                                      } else if (P->type() == xAOD::Type::ObjectType::Tau) {
                                          return m_tau_selection->GetTauDecorations()->passSignal(*P);
                                      }
                                      return false;
                                  },
                                  2);
        m_ParticleConstructor->DetachSubContainer();
        // Invariant dilepton Candidates
        static ParticleStorage* DiLepStore = m_XAMPPInfo->GetParticleStorage("dilepton");
        ATH_CHECK(DiLepStore->Fill(m_ParticleConstructor->GetSubContainer("DiLepCandidates")));

        if (!isData()) {
            static XAMPP::Storage<int>* dec_LQ_ncl = m_XAMPPInfo->GetVariableStorage<int>("LQ_ncl");
            int ncl_total = -1;
            int dsid = m_XAMPPInfo->mcChannelNumber();
            if ((dsid >= 310550 && dsid <= 310559) || (dsid >= 312201 && dsid <= 312242)) {
                // 3rd gen. LQ pair-production samples
                ncl_total = 0;
                for (const auto tbsm : *m_truth_selection->GetTruthBSM()) {
                    if (abs(tbsm->pdgId()) != 43 && abs(tbsm->pdgId()) != 42) continue;
                    int prevChildPdgId = 0;
                    int ncl = 0;
                    for (size_t c = 0; c < tbsm->nChildren(); ++c) {
                        const xAOD::TruthParticle* child = tbsm->child(c);
                        if (!child) continue;
                        if (child->pdgId() == tbsm->pdgId()) {
                            ncl = 0;
                            break;
                        }
                        if (!child->isLepton()) continue;
                        if (child->isChLepton()) {
                            if (prevChildPdgId == -1 * child->pdgId()) {
                                ncl--;
                                prevChildPdgId = 0;
                            } else {
                                ncl++;
                                prevChildPdgId = child->pdgId();
                            }
                        } else if (abs(prevChildPdgId + child->pdgId()) == 1) {
                            ncl--;
                            prevChildPdgId = 0;
                        }
                    }
                    ncl_total += ncl;
                }
            }
            ATH_CHECK(dec_LQ_ncl->Store(ncl_total));
        }

        return StatusCode::SUCCESS;
    }
}  // namespace XAMPP
