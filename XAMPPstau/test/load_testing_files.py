from ClusterSubmission.Utils import ResolvePath, ReadListFromFile, CreateDirectory, id_generator, RecursiveLS, IsROOTFile, WriteList
import os, logging, argparse


def setup_parser():
    parser = argparse.ArgumentParser(
        description="Script to download the files from eos. This should only be considered in docker containers",
        formatter_class=argparse.ArgumentDefaultsHelpFormatter)

    parser.add_argument('--in_file', help='Path to the reference file', required=True)
    parser.add_argument('--out_file', help='Path to the file from the CI', default="file_list.txt")
    parser.add_argument("--download_dir", help="output_directory", default="/file_tmp/")
    return parser


if __name__ == '__main__':
    options = setup_parser().parse_args()
    CreateDirectory(options.download_dir, False)
    out_file = []
    if options.in_file.startswith("root://"):
        logging.info("EOS file %s parsed to the job" % (options.in_file))
        if os.system("xrdcp %s %s" % (options.in_file, options.download_dir)):
            logging.error("Failed to download %s" % (options.in_file))
            exit(1)
        out_file = RecursiveLS(options.download_dir)
    ## Locally accessible file
    elif os.path.isdir(options.in_file):
        out_file = [f for f in RecursiveLS(options.download_dir) if IsROOTFile(f)]
    elif os.path.isfile(options.in_file) and IsROOTFile(options.in_file):
        out_file = [options.in_file]
    ### Assume a txt file
    elif os.path.isfile(options.in_file):
        in_txt = ReadListFromFile(options.in_file)
        for x in in_txt:
            if x.startswith("root://") and os.system("xrdcp %s %s" % (options.in_file, options.download_dir)):
                logging.error("Failed to download %s" % (x))
                exit(1)
            elif os.path.isfile(x):
                out_file += [x]
            elif os.path.isdir(x):
                out_file += [x + "/" + y for y in os.listdir(x) if IsROOTFile(y)]
            else:
                logging.error("Do not know what to do with %s" % (x))
                exit(1)

    WriteList(out_file, options.out_file)
