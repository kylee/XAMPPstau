#!/env/python
import os, sys
from ClusterSubmission.Utils import ResolvePath, WriteList, id_generator
from XAMPPstau.SubmitToGrid import getLogicalDataSets
import getpass

PROD = "V30"
USER = "jojungge"
UPDATE = False
SEND = False
MONITOR = True

Failed = []
wrong_jobs = []
SampleDir = ResolvePath("XAMPPstau/SampleLists/mc16_13TeV/lephad/")
Update_Cmd = "python %s --ListDir %s --derivation SUSY18" % (ResolvePath("XAMPPbase/python/UpdateSampleLists.py"), SampleDir)
if UPDATE: os.system(Update_Cmd)

#submit
Cmd = "python %s --stream LepHad --sampleList %s --production %s %s --productionRole perf-muons --noMergeJob" % (
    ResolvePath("XAMPPstau/python/SubmitToGrid.py"), " ".join(
        ["%s/%s" % (SampleDir, S)
         for S in os.listdir(SampleDir)]), PROD, "" if len(Failed) == 0 else "--physicalSamples %s" % (" ".join(Failed)))
if SEND: os.system(Cmd)

#monitor
Monitor_Cmd = "python %s --stream LepHad --production %s --replicate_ds --destRSE MPPMU_LOCALGROUPDISK --rucio %s --productionRole perf-muons --automatic_retry --change_files_per_job 1 %s" % (
    ResolvePath("XAMPPstau/python/checkProduction.py"), PROD, USER, "" if len(wrong_jobs) == 0 else "--exclude_jobids %s" %
    (" ".join([str(i) for i in wrong_jobs])))
if MONITOR: os.system(Monitor_Cmd)
