#include <XAMPPbase/AnalysisUtils.h>
#include <XAMPPbase/ToolHandleSystematics.h>

#include <XAMPPstau/AnalysisUtils.h>
#include <XAMPPstau/StauJigSawWModule.h>

namespace XAMPP {
    StauJigSawWModule::~StauJigSawWModule() {}
    StauJigSawWModule::StauJigSawWModule(const std::string& myname) :
        StauJigSawZModule(myname),
        m_lep(nullptr),
        m_nu(nullptr),
        m_W(nullptr),
        m_dec_cosThetaStar(nullptr),
        m_dec_dPhi(nullptr),
        m_dec_GammaBeta(nullptr)

    {}
    unsigned int StauJigSawWModule::flavour() const { return JigSawFlavour::W; }
    StatusCode StauJigSawWModule::bookVariables() {
        ATH_CHECK(retrieveContainers());
        ATH_CHECK(bookParticleStore("JigSawCandidates", m_JigSaws, true));
        ATH_CHECK(m_JigSaws->SaveVariable<int>("pdgId"));
        ATH_CHECK(m_JigSaws->SaveVariable<unsigned int>("frame"));

        ATH_CHECK(newVariable("CosThetaStarW", -2, m_dec_cosThetaStar));
        ATH_CHECK(newVariable("dPhiDecayPlaneW", 10 * std::acos(-1), m_dec_dPhi));
        ATH_CHECK(newVariable("GammaBetaW", -1, m_dec_GammaBeta));
        ATH_CHECK(setupRJanalysis());
        return StatusCode::SUCCESS;
    }
    StatusCode StauJigSawWModule::setupRJanalysis() {
        m_lep = std::make_shared<VisibleRecoFrame>("lep", "#it{L}");
        m_nu = std::make_shared<InvisibleRecoFrame>("nu", "#nu_{1}");
        m_W = std::make_shared<DecayRecoFrame>("W", "W #to #it{L}#nu");
        m_W->AddChildFrame(*m_lep);
        m_W->AddChildFrame(*m_nu);

        m_lab_frame = std::make_shared<RestFrames::LabRecoFrame>("LabFrame", "LabFrame");
        m_lab_frame->SetChildFrame(*m_W);
        if (!m_lab_frame->InitializeTree()) return StatusCode::FAILURE;

        ATH_MSG_INFO("Decay tree successfully built");
        m_InvisGroup = std::make_shared<InvisibleGroup>("Invisible", "#nu - jigsaws");
        m_InvisGroup->AddFrame(*m_nu);
        ATH_MSG_INFO("Invisible group built");
        // The invariant mass of the invisible system can be choosen
        // to be the smallest Lorentz invariant function of the visible Masses
        m_inivisblemass = std::make_shared<SetMassInvJigsaw>("X1_Mass", "Set M_{#nu_{1}, #nu_{2}} to minimum");

        // The rapidity of the visible system can be choosen to be equal to the rapidity of the visible
        // particles
        std::shared_ptr<SetRapidityInvJigsaw> rapidity = std::make_shared<SetRapidityInvJigsaw>("Rapidity", "Equal rapidity");
        m_rapidity = rapidity;
        rapidity->AddVisibleFrame(*m_lep);

        m_InvisGroup->AddJigsaw(*m_inivisblemass);
        m_InvisGroup->AddJigsaw(*m_rapidity);
        if (!m_lab_frame->InitializeAnalysis()) return StatusCode::FAILURE;

        return StatusCode::SUCCESS;
    }
    StatusCode StauJigSawWModule::fill() {
        ATH_CHECK(fillDefaultValues());
        xAOD::IParticleContainer* electrons = m_electrons->Container();
        xAOD::IParticleContainer* muons = m_muons->Container();

        if (!m_ParticleConstructor->HasSubContainer(full_name("JigSawParticles"))) {
            ATH_CHECK(m_ParticleConstructor->CreateSubContainer(full_name("JigSawParticles")));
            ATH_CHECK(m_JigSaws->Fill(m_ParticleConstructor->GetSubContainer(full_name("JigSawParticles"))));
        } else
            ATH_CHECK(m_ParticleConstructor->LoadSubContainer(full_name("JigSawParticles")));

        const xAOD::IParticle* P1 = nullptr;
        // No leptons in the event. Nothing of particular interst
        if (muons->empty() && electrons->empty()) {
            return StatusCode::SUCCESS;
        } else if (muons->empty()) {
            P1 = electrons->at(0);
        } else {
            P1 = muons->at(0);
        }

        // There are more than one muon or electron in the event. This one is not of interest neither
        if (muons->size() > 1 || electrons->size() > 1) {
            ATH_MSG_DEBUG("Skip event because it's not part of our topology");
            return StatusCode::SUCCESS;
        }
        m_lab_frame->ClearEvent();
        // Assign the missing-transverse eneergy
        xAOD::MissingET* met = m_met->GetValue();
        TVector3 met_TLV;
        met_TLV.SetPtEtaPhi(met->met(), 0, met->phi());
        m_InvisGroup->SetLabFrameThreeVector(met_TLV);

        // Define the lepton in the event
        m_lep->SetLabFrameFourVector(P1->p4());
        m_lep->SetCharge(TypeToPdgId(P1));

        if (!m_lab_frame->AnalyzeEvent()) {
            ATH_MSG_DEBUG("Event analysis did not work out");
            return StatusCode::SUCCESS;
        }
        saveInFrame(*m_nu, (std::fabs(m_lep->GetCharge()) + 1) * Sign(m_lep->GetCharge()), DecayFrame::Lab_Frame);
        saveInFrame(*m_nu, (std::fabs(m_lep->GetCharge()) + 1) * Sign(m_lep->GetCharge()), DecayFrame::PP_Frame);
        xAOD::Particle* v_W_LAB = saveInFrame(*m_W, 24 * Sign(m_lep->GetCharge()), DecayFrame::Lab_Frame);

        float cosThetaStar = m_W->GetCosDecayAngle(*m_lab_frame);
        float decayAngle = m_W->GetDeltaPhiDecayAngle();
        ATH_CHECK(m_dec_cosThetaStar->Store(IsFinite(cosThetaStar) ? cosThetaStar : -10 * std::acos(-1)));
        ATH_CHECK(m_dec_dPhi->Store(IsFinite(decayAngle) ? decayAngle : -10 * std::acos(-1)));
        ATH_CHECK(m_dec_GammaBeta->Store(v_W_LAB->pt() / v_W_LAB->m()));

        return StatusCode::SUCCESS;
    }
    RecoFrame_Ptr StauJigSawWModule::getRestFrame(int frame) const {
        if (frame == DecayFrame::Lab_Frame) return m_lab_frame;
        if (frame == DecayFrame::PP_Frame) return m_W;
        ATH_MSG_WARNING("Could not resolve the restframe " << frame);
        return RecoFrame_Ptr();
    }

}  // namespace XAMPP
