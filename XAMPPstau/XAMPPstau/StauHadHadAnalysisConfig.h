#ifndef XAMPPStau_StauHadHadAnalysisConfig_h
#define XAMPPStau_StauHadHadAnalysisConfig_h
#include <XAMPPbase/AnalysisConfig.h>

namespace XAMPP {
    class StauHadHadAnalysisConfig : public AnalysisConfig {
    public:
        ASG_TOOL_CLASS(StauHadHadAnalysisConfig, XAMPP::IAnalysisConfig)
        StauHadHadAnalysisConfig(std::string Analysis = "Staus");
        virtual ~StauHadHadAnalysisConfig() = default;

    protected:
        virtual StatusCode initializeCustomCuts();
    };
}  // namespace XAMPP
#endif
