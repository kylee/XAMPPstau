#include <XAMPPbase/AnalysisUtils.h>
#include <XAMPPbase/ToolHandleSystematics.h>

#include <XAMPPstau/AnalysisUtils.h>
#include <XAMPPstau/EventShapeModule.h>

#include <EventShapeVariablesCalc/EventShapeVariablesCalc.h>
#include <SUSYTools/SUSYObjDef_xAOD.h>

namespace XAMPP {
    EventShapeModule::~EventShapeModule() {}
    EventShapeModule::EventShapeModule(std::string myname) :
        StauAnalysisModule(myname),
        m_EventShapeCalc(),
        m_Evshape_Sphericity(true),
        m_Evshape_Spherocity(true),
        m_Evshape_Thrust(true),

        m_Evshape_FoxWolfram(true),
        m_norm_FoxWolfram(true),
        m_maxWolframMoments(7),
        m_WolframMoments(),
        m_valid_wolfram(nullptr),
        m_Sphericity(nullptr),
        m_Planarity(nullptr),
        m_Aplanarity(nullptr),
        m_valid_sphericity(nullptr),

        m_Lin_Sphericity(nullptr),
        m_Lin_Sphericity_C(nullptr),
        m_Lin_Sphericity_D(nullptr),
        m_Lin_Planarity(nullptr),
        m_Lin_Aplanarity(nullptr),
        m_valid_Lin_sphericity(nullptr),

        m_Thrust(nullptr),
        m_ThrustMajor(nullptr),
        m_ThrustMinor(nullptr),
        m_Oblateness(nullptr),
        m_valid_thrust(nullptr),

        m_Spherocity(nullptr),
        m_valid_Spherocity(nullptr),
        m_circularity(nullptr),
        m_Lin_circularity(nullptr) {
        m_EventShapeCalc = std::unique_ptr<EventShapeVariablesCalc>(new EventShapeVariablesCalc());
        declareProperty("CalculateSphericity", m_Evshape_Sphericity);
        declareProperty("CalculateSpherocity", m_Evshape_Spherocity);
        declareProperty("CalculateThrust", m_Evshape_Thrust);
        declareProperty("CalculateFoxWolfram", m_Evshape_FoxWolfram);
        declareProperty("OrderWolframMoments", m_maxWolframMoments);
        declareProperty("NormalizeWolframMoments", m_norm_FoxWolfram);
    }

    StatusCode EventShapeModule::bookVariables() {
        ATH_CHECK(retrieveContainers());
        if (m_Evshape_Sphericity) {
            m_EventShapeCalc->do_sphericity();
            ATH_CHECK(newVariable("Sphericity", m_Sphericity));
            ATH_CHECK(newVariable("Planarity", m_Planarity));
            ATH_CHECK(newVariable("Aplanarity", m_Aplanarity));
            ATH_CHECK(newVariable("Sphericity_IsValid", m_valid_sphericity));
            ATH_CHECK(newVariable("Lin_Sphericity", m_Lin_Sphericity));
            ATH_CHECK(newVariable("Lin_Sphericity_C", m_Lin_Sphericity_C));
            ATH_CHECK(newVariable("Lin_Sphericity_D", m_Lin_Sphericity_D));
            ATH_CHECK(newVariable("Lin_Planarity", m_Lin_Planarity));
            ATH_CHECK(newVariable("Lin_Aplanarity", m_Lin_Aplanarity));
            ATH_CHECK(newVariable("Lin_Sphericity_IsValid", m_valid_Lin_sphericity));

            ATH_CHECK(newVariable("Circularity", m_circularity));
            ATH_CHECK(newVariable("Lin_Circularity", m_Lin_circularity));
        }
        if (m_Evshape_Thrust) {
            m_EventShapeCalc->do_thrust();
            ATH_CHECK(newVariable("Thrust", m_Thrust));
            ATH_CHECK(newVariable("ThrustMajor", m_ThrustMajor));
            ATH_CHECK(newVariable("ThrustMinor", m_ThrustMinor));
            ATH_CHECK(newVariable("Oblateness", m_Oblateness));
            ATH_CHECK(newVariable("Thrust_IsValid", m_valid_thrust));
        }
        if (m_Evshape_Spherocity) {
            m_EventShapeCalc->do_spherocity();
            ATH_CHECK(newVariable("Spherocity", m_Spherocity));
            ATH_CHECK(newVariable("Spherocity_IsValid", m_valid_Spherocity));
        }
        if (m_Evshape_FoxWolfram) {
            m_EventShapeCalc->do_fox_wolfram();
            m_EventShapeCalc->use<FoxWolfram>()->set_max_moments(m_maxWolframMoments);
            if (m_norm_FoxWolfram) m_EventShapeCalc->use<FoxWolfram>()->normalize_to_zeroth_moment();

            ATH_CHECK(newVariable("WolframMoments_AreValid", m_valid_wolfram));
            for (unsigned int f = 0; f < m_EventShapeCalc->use<FoxWolfram>()->n_moments(); ++f) {
                XAMPP::Storage<float>* store = nullptr;
                ATH_CHECK(newVariable(Form("WolframMoment_%d", f), store));
                m_WolframMoments.push_back(store);
            }
        }

        return StatusCode::SUCCESS;
    }
    StatusCode EventShapeModule::fill() {
        // Add object to the eventshape calculator and calulate everything
        m_EventShapeCalc->reset();
        m_EventShapeCalc->add(m_electrons->Container());
        m_EventShapeCalc->add(m_muons->Container());
        m_EventShapeCalc->add(m_taus->Container());
        m_EventShapeCalc->add(m_jets->Container());
        m_EventShapeCalc->add(m_met->GetValue());
        m_EventShapeCalc->calculate();
        // Fill everything
        if (m_Evshape_Sphericity) {
            ATH_CHECK(m_valid_sphericity->Store(m_EventShapeCalc->use<Sphericity>()->sphericity_calculation_is_valid()));
            ATH_CHECK(m_Sphericity->Store(m_EventShapeCalc->use<Sphericity>()->sphericity()));

            ATH_CHECK(m_Planarity->Store(m_EventShapeCalc->use<Sphericity>()->planarity()));
            ATH_CHECK(m_Aplanarity->Store(m_EventShapeCalc->use<Sphericity>()->aplanarity()));

            ATH_CHECK(m_valid_Lin_sphericity->Store(m_EventShapeCalc->use<Sphericity>()->linearized_sphericity_calculation_is_valid()));
            ATH_CHECK(m_Lin_Sphericity->Store(m_EventShapeCalc->use<Sphericity>()->linearized_sphericity()));
            ATH_CHECK(m_Lin_Sphericity_C->Store(m_EventShapeCalc->use<Sphericity>()->linearized_sphericity_c()));
            ATH_CHECK(m_Lin_Sphericity_D->Store(m_EventShapeCalc->use<Sphericity>()->linearized_sphericity_d()));

            ATH_CHECK(m_Lin_Planarity->Store(m_EventShapeCalc->use<Sphericity>()->linearized_planarity()));
            ATH_CHECK(m_Lin_Aplanarity->Store(m_EventShapeCalc->use<Sphericity>()->linearized_aplanarity()));

            ATH_CHECK(m_circularity->Store(m_EventShapeCalc->use<Sphericity>()->circularity()));
            ATH_CHECK(m_Lin_circularity->Store(m_EventShapeCalc->use<Sphericity>()->linearized_circularity()));
        }
        if (m_Evshape_Spherocity) {
            ATH_CHECK(m_Spherocity->Store(m_EventShapeCalc->use<Spherocity>()->spherocity()));
            ATH_CHECK(m_valid_Spherocity->Store(m_EventShapeCalc->use<Spherocity>()->calculation_is_valid()));
        }
        if (m_Evshape_Thrust) {
            ATH_CHECK(m_Thrust->Store(m_EventShapeCalc->use<Thrust>()->thrust()));
            ATH_CHECK(m_ThrustMajor->Store(m_EventShapeCalc->use<Thrust>()->thrustMajor()));
            ATH_CHECK(m_ThrustMinor->Store(m_EventShapeCalc->use<Thrust>()->thrustMinor()));
            ATH_CHECK(m_Oblateness->Store(m_EventShapeCalc->use<Thrust>()->oblateness()));
            ATH_CHECK(m_valid_thrust->Store(m_EventShapeCalc->use<Thrust>()->calculation_is_valid()));
        }
        if (m_Evshape_FoxWolfram) {
            ATH_CHECK(m_valid_wolfram->Store(m_EventShapeCalc->use<FoxWolfram>()->calculation_is_valid()));
            for (unsigned int f = 0; f < m_EventShapeCalc->use<FoxWolfram>()->n_moments(); ++f) {
                ATH_CHECK(
                    m_WolframMoments.at(f)->Store(m_valid_wolfram->GetValue() ? m_EventShapeCalc->use<FoxWolfram>()->moment(f) : FLT_MAX));
            }
        }

        return StatusCode::SUCCESS;
    }

}  // namespace XAMPP
