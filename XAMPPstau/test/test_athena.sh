#!/bin/bash

# definition of folder for storing test results
TESTDIR="${jobName}_${STREAM}/"

EOS_PATH="root://eoshome.cern.ch//eos/user/x/xampp/ci/stau/"
# create directory for results
mkdir -p ${TESTDIR}
cd ${TESTDIR}
# get kerberos token
if [ -z ${SERVICE_PASS} ]; then
  echo "You did not set the environment variable SERVICE_PASS.\n\
Please define in the gitlab project settings/CI the secret variables SERVICE_PASS and CERN_USER."
  exit 1
else
  echo "${SERVICE_PASS}" | kinit ${CERN_USER}@CERN.CH
fi

echo "" > FileList.txt
while read -r to_process; do
  file_name=${to_process#*/}
  if [ ! -f "${file_name}" ]; then
     echo "File ${to_process} not found! Copying it from EOS"
     echo "xrdcp ${EOS_PATH}${to_process} ${PWD}/${file_name}"
     xrdcp "${EOS_PATH}${to_process}" ${PWD}/${file_name}
     if [ $? -ne 0 ];then
        echo "Download failed"
        exit 1
    fi
     echo "${PWD}/${file_name}" >> FileList.txt
  fi
done < "/xampp/XAMPPstau/XAMPPstau/test/datasamples/${jobName}.txt" 
echo "#########################################################################"
ls -lh
echo "#########################################################################"

##############################
# Process test sample        #
##############################

# run the job
echo "python ${TestArea}/XAMPPstau/python/runAthena.py --parseFilesForPRW --filesInput FileList.txt --outFile AnalysisOutput.root --jobOptions ${JOBOPTIONS} --noSyst"
python ${TestArea}/XAMPPstau/python/runAthena.py  --parseFilesForPRW --filesInput FileList.txt --outFile AnalysisOutput.root --jobOptions ${JOBOPTIONS} --noSyst
###################################################
# Raise error if execution failed                 #
###################################################
if [ $? -ne 0 ]; then
  printf '%s\n' "Execution of runAthena.py failed" >&2  # write error message to stderr
  exit 1
fi
mkdir -p ${CI_PROJECT_DIR}
cp AnalysisOutput.root ${CI_PROJECT_DIR}/AnalysisOutput.root
##############################
# Evalulate cut flows        #
##############################
if [ -z "${CutFlows}" ];then
   echo "No CutFlows variable is set"
   exit 1
fi
for R in ${CutFlows}; do
    python ${TestArea}/XAMPPbase/XAMPPbase/python/printCutFlow.py -i AnalysisOutput.root -a ${R} | tee /xampp/XAMPPstau/${jobName}_${STREAM}_${R}.txt
    if [ $? -ne 0 ];then
        echo "Failed to retrieve raw cutflow for ${R}"
        exit 1
    fi
    cp /xampp/XAMPPstau/${jobName}_${STREAM}_${R}.txt ${CI_PROJECT_DIR}

    if [[ $jobName != *data* ]]; then
      python ${TestArea}/XAMPPbase/XAMPPbase/python/printCutFlow.py -i AnalysisOutput.root -a ${R} --weighted | tee /xampp/XAMPPstau/${jobName}_${STREAM}_${R}_weighted.txt
      if [ $? -ne 0 ];then
          echo "Failed to retrieve weighted cutflow for ${R}"
          exit 1
      fi
      cp /xampp/XAMPPstau/${jobName}_${STREAM}_${R}_weighted.txt ${CI_PROJECT_DIR}    
    fi
done

for R in ${CutFlows};do
    diff /xampp/XAMPPstau/XAMPPstau/test/reference_cutflows/${jobName}_${STREAM}_${R}.txt /xampp/XAMPPstau/${jobName}_${STREAM}_${R}.txt
    if [ $? -ne 0 ];then
        echo "The raw cutflows do not match for ${R}"
        exit 1
    fi
#    diff /xampp/XAMPPstau/XAMPPstau/test/reference_cutflows/${jobName}_${STREAM}_${R}_weighted.txt /xampp/XAMPPstau/${jobName}_${STREAM}_${R}_weighted.txt
#    if [ $? -ne 0 ];then
#        echo "The weighted cutflows do not match for ${R}"
#        exit 1
#    fi
done

