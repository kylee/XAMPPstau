#! /usr/bin/env python
import argparse
from XAMPPbase.runAthena import ExecuteAthena, AssembleAthenaOptions
from XAMPPbase.AthArgParserSetup import SetupArgParser
if __name__ == "__main__":
    parser = SetupArgParser()
    parser.set_defaults(jobOptions="XAMPPstau/runLepHad.py")
    parser.add_argument("--doTauPromotion", help="Enable tau promotion on the fly", action="store_true", default=False)
    RunOptions = parser.parse_args()
    AthenaArgs = AssembleAthenaOptions(RunOptions, parser)
    ExecuteAthena(RunOptions, AthenaArgs)
