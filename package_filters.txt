# add the XAMPPbase with its subpackages to the repository
+ XAMPPbase/XAMPPbase
+ XAMPPbase/CalcGenericMT2
+ XAMPPbase/IFFTruthClassifier
# but dont add anything else from that repo
- XAMPPbase.*
# add the XAMPPplotting repo
+ XAMPPplotting
+ XAMPPplotting/ClusterSubmission
# Add the EventShapeVariables package, too
+ EventShapeVariables/EventShapeVariablesCalc
- EventShapeVariables.*
#+ athena/PhysicsAnalysis/SUSYPhys/SUSYTools
#- athena.*

