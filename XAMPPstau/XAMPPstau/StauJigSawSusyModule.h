#ifndef StauJigSawSusyModule_H
#define StauJigSawSusyModule_H

#include <XAMPPstau/StauJigSawZModule.h>

namespace XAMPP {
    //-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
    //  Recusrive Jigsaw reconstruction, described in https://arxiv.org/abs/1705.10733       =
    //                                 \stau_{1}\stau_{2}                                    =
    //                             /                       \                                 =
    //                            /                         \                                =
    //                           /                           \                               =
    //                          /                             \                              =
    //                         /                               \                             =
    //                  stau{1}                                 stau{2}                      =
    //                 /      \                                /      \                      =
    //                /        \                              /        \                     =
    //               /          \                            /          \                    =
    //           tau_{1}      \ninoone_{1}               tau_{2}      \ninoone_{2}           =
    //          /       \                               /       \                            =
    //  tau_{1}^{vis}   nu_{1}                  tau_{2}^{vis}   nu_{2}                       =
    //-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=

    class StauJigSawSusyModule : public StauJigSawZModule {
    public:
        // Create a proper constructor for Athena
        ASG_TOOL_CLASS(StauJigSawSusyModule, XAMPP::IAnalysisModule)
        //
        StauJigSawSusyModule(const std::string& myname);
        virtual ~StauJigSawSusyModule();

        virtual StatusCode fill();

        virtual unsigned int flavour() const;

    private:
        virtual StatusCode setupRJanalysis();

        VisibleRecoFrame_Ptr m_vis_Tau1;
        VisibleRecoFrame_Ptr m_vis_Tau2;

        RecoFrame_Ptr m_nu1;
        RecoFrame_Ptr m_nu2;

        RecoFrame_Ptr m_Tau1;
        RecoFrame_Ptr m_Tau2;

        RecoFrame_Ptr m_Stau1;
        RecoFrame_Ptr m_Stau2;

        RecoFrame_Ptr m_Nino1;
        RecoFrame_Ptr m_Nino2;

        RecoFrame_Ptr m_Stau1Stau2;

        CombGrp_Ptr m_comb_jigsaw;

        Jigsaw_Ptr m_min_dM_tau1;
        Jigsaw_Ptr m_min_dM_tau2;
    };
}  // namespace XAMPP
#endif
