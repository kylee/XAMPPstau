Extracting MetaData information...
Having found CutFlow for MC, printing...
Printing raw events
CutFlowHisto               DSID_364159_CutFlow
Systematic variation       Nominal   
##############################################
Cut                        Yields    
##############################################
Initial                    84614 ± 290.88
PassGRL                    84614 ± 290.88
passLArTile                84614 ± 290.88
Trigger                    68734 ± 262.17
HasVtx                     68734 ± 262.17
BadJet                     68455 ± 261.64
CosmicMuon                 68410 ± 261.55
BadMuon                    68319 ± 261.38
TrigMatching               60050 ± 245.05
N_{#mu}^{baseline} #geq 1  58471 ± 241.81
N_{#mu}^{loose} = 0        57618 ± 240.04
N_{e}^{baseline} = 0       56784 ± 238.29
N_{#tau}^{baseline}>=1     13654 ± 116.85
E_{T}^{miss} > 15 GeV      12567 ± 112.10
N_{#tau}^{signal}==0       10661 ± 103.25
N_{bjet} == 0              10308 ± 101.53
Final                      10308 ± 101.53
##############################################
Printing raw events
CutFlowHisto               DSID_364160_CutFlow
Systematic variation       Nominal   
##############################################
Cut                        Yields    
##############################################
Initial                    28886 ± 169.96
PassGRL                    28886 ± 169.96
passLArTile                28886 ± 169.96
Trigger                    23906 ± 154.62
HasVtx                     23906 ± 154.62
BadJet                     23830 ± 154.37
CosmicMuon                 23792 ± 154.25
BadMuon                    23737 ± 154.07
TrigMatching               20715 ± 143.93
N_{#mu}^{baseline} #geq 1  20338 ± 142.61
N_{#mu}^{loose} = 0        19895 ± 141.05
N_{e}^{baseline} = 0       19479 ± 139.57
N_{#tau}^{baseline}>=1     4151  ± 64.43
E_{T}^{miss} > 15 GeV      3798  ± 61.63
N_{#tau}^{signal}==0       3273  ± 57.21
N_{bjet} == 0              2904  ± 53.89
Final                      2904  ± 53.89
##############################################
Printing raw events
CutFlowHisto               DSID_364161_CutFlow
Systematic variation       Nominal   
##############################################
Cut                        Yields    
##############################################
Initial                    39163 ± 197.90
PassGRL                    39163 ± 197.90
passLArTile                39163 ± 197.90
Trigger                    31587 ± 177.73
HasVtx                     31587 ± 177.73
BadJet                     31482 ± 177.43
CosmicMuon                 31175 ± 176.56
BadMuon                    31094 ± 176.33
TrigMatching               26700 ± 163.40
N_{#mu}^{baseline} #geq 1  26049 ± 161.40
N_{#mu}^{loose} = 0        25024 ± 158.19
N_{e}^{baseline} = 0       24126 ± 155.33
N_{#tau}^{baseline}>=1     5466  ± 73.93
E_{T}^{miss} > 15 GeV      5049  ± 71.06
N_{#tau}^{signal}==0       4306  ± 65.62
N_{bjet} == 0              3494  ± 59.11
Final                      3494  ± 59.11
##############################################
