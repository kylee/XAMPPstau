#!/bin/bash

source /home/atlas/release_setup.sh
# get kerberos token
if [ -z ${SERVICE_PASS} ]; then
  echo "You did not set the environment variable SERVICE_PASS.\n\
Please define in the gitlab project settings/CI the secret variables SERVICE_PASS and CERN_USER."
else
  echo "${SERVICE_PASS}" | kinit ${CERN_USER}@CERN.CH
fi

url_host="gitlab.cern.ch/atlas-mpp-xampp/XAMPPstau.git"
if [ -z "${GIT_AUTHTOKEN}" ]; then
     echo "Use the login credentials to push the changes"
    git remote set-url origin https://"${CERN_USER}":"${SERVICE_PASS}"@${url_host}
else
      echo "Use the gitlab token to push the changes"
      git remote set-url origin "https://gitlab-ci-token:${GIT_AUTHTOKEN}@${url_host}"
fi  

export SERVICE_PASS=""
ls -lh
cp *_LepHad_*.txt XAMPPstau/test/reference_cutflows/
cp *_HadHad_*.txt XAMPPstau/test/reference_cutflows/

git commit XAMPPstau/test/ -m 'Updated reference cutflows'
git remote show origin
git push origin ${CI_COMMIT_REF_NAME} 

exit 1
 
